from setuptools import setup, find_packages
from pttrss import __productname__, __version__, __dev_status__


with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name=__productname__,
    version=__version__,
    description="Tiny Tiny RSS bindings for Python with no dependencies",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Etienne Carichon",
    author_email="haluxluxeay@proton.me",
    license="GPL3",
    packages=find_packages(),
    include_package_data=True,
    classifiers=[
        __dev_status__,
        "Environment :: Web Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License (GPL)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
)
