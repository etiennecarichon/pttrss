PROJECT_PATH=./
TEST_PATH=$(PROJECT_PATH)/tests
MAX_LEN=79

clean: clean-build clean-pyc

clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +
	find . -name 'tags' -delete

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

lint:
	yapf3 -i $(PROJECT_PATH)/*.py $(TEST_PATH)/*.py

tags:
	find . -name "*.py" -execdir \
		ctags -a -f tags --c++-kinds=+p --fields=+iaS \
		--extra=+q {} \;

test:
	python3 -m unittest discover $(TEST_PATH)
