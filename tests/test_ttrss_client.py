import json
from urllib.request import OpenerDirector
from unittest import TestCase
from unittest.mock import mock_open, patch

from pttrss.client import TTRSSSessionHandler, TTRSSClient, API_PATH
from pttrss.exceptions import TTRSSMissingAuthentification, TTRSSMissingOP

URL = "http://rss.unittest.info"
TTRSS_API = URL + API_PATH
FAKE_SID = "xxxxxxxxxxxxxxxxxxxxxxxxxx"
USER = "user"
PASS = "password1234"

LOGIN_ERROR = {'seq': 0, 'status': 1, 'content': {'error': 'LOGIN_ERROR'}}
LOGIN_RESP = {
    "seq": 0,
    "status": 0,
    "content": {
        "session_id": FAKE_SID,
        "config": {
            "icons_dir": "feed-icons",
            "icons_url": "feed-icons",
            "daemon_is_running": False,
            "custom_sort_types": [],
            "num_feeds": 85,
        },
        "api_level": 17,
    },
}


class TestRequest(TestCase):

    VAL = bytes(json.dumps(LOGIN_RESP), "UTF-8")
    ERR = bytes(json.dumps(LOGIN_ERROR), "UTF-8")

    @patch.object(OpenerDirector,
                  'open',
                  new_callable=mock_open,
                  read_data=VAL)
    def test__data_parser_login_response(self, mock_op):
        """'_data_parser' returns data from HTTP request"""
        data = {"op": "login", "user": "foo", "password": "bar"}
        self.assertEqual(TTRSSClient._data_parser("foo", data=data),
                         LOGIN_RESP)

    @patch.object(OpenerDirector,
                  'open',
                  new_callable=mock_open,
                  read_data=ERR)
    def test__data_parser_login_error(self, mock_op):
        """'_data_parser' returns data from HTTP request with login error as 
        response"""
        data = {"op": "login", "user": "foo", "password": "bar"}
        self.assertEqual(TTRSSClient._data_parser("foo", data=data),
                         LOGIN_ERROR)

    @patch.object(OpenerDirector,
                  'open',
                  new_callable=mock_open,
                  read_data=VAL)
    def test__data_parser_is_dict(self, mock_op):
        """'_data_parser' returns a dict from HTTP request"""
        data = {"op": "login", "user": "foo", "password": "bar"}
        self.assertIsInstance(TTRSSClient._data_parser("foo", data=data), dict)


class TestResponseHandlers(TestCase):
    def setUp(self):
        self.ttrss_session = TTRSSSessionHandler(URL, USER, PASS)
        self.client = TTRSSClient(self.ttrss_session)

    def test_handle_status_when_succed(self):
        """'_handle_status' returns True if response is valid"""
        self.assertTrue(self.client._handle_status(LOGIN_RESP))

    def test_handle_status_when_failed(self):
        """'_handle_status' returns False if response as an error status"""
        self.assertFalse(self.client._handle_status(LOGIN_ERROR))

    def test_handle_status_with_invalid_type(self):
        """'data' in '_handle_status' must be from dict type, returns TypeError 
        if not"""
        with self.assertRaises(TypeError):
            self.client._handle_status("foo")

    def test_handle_status_with_invalid_data(self):
        """'API_STATUS' must be find in '_handle_status', returns False if not
        find"""
        self.assertFalse(self.client._handle_status({"foo": "bar"}))


class TestDataRequirement(TestCase):
    def setUp(self):
        self.ttrss_session = TTRSSSessionHandler(URL, USER, PASS)
        self.client = TTRSSClient(self.ttrss_session)
        self.data = [{
            "op": "login"
        }, {
            "op": "login",
            "user": "foo"
        }, {
            "op": "login",
            "password": "bar"
        }]

    def test_data_with_valids_requirement(self):
        """'_test_requirements' returns True if data meets requirements"""
        data = {"op": "login", "user": "foo", "password": "bar"}
        self.assertTrue(self.client._test_requirements(data))

    def test_data_with_missing_op(self):
        """'_test_requirements' must returns TTRSSMissingOP when missing OP code
        """
        data = {"user": "foo", "password": "bar"}
        with self.assertRaises(TTRSSMissingOP):
            self.client._test_requirements(data)

    def test_data_with_missing_id(self):
        """'_test_requirements' must returns TTRSSMissingAuthentification when 
        there is no sid or user/password"""
        for dic in self.data:
            with self.assertRaises(TTRSSMissingAuthentification):
                self.client._test_requirements(dic)

    def test_data_with_sid(self):
        """'_test_requirements' must use SID if parsed"""
        for dic in self.data:
            self.assertTrue(self.client._test_requirements(dic, FAKE_SID))


class TestMisc(TestCase):
    def setUp(self):
        self.ttrss_session = TTRSSSessionHandler(URL, USER, PASS)
        self.client = TTRSSClient(self.ttrss_session)

    def test_is_dict_when_true(self):
        """'_is_dickey' returns True if 'key' is in 'dic'"""
        dic = {"key": "value"}
        self.assertTrue(self.client._is_dickey(dic, "key"))

    def test_is_dict_when_false(self):
        """'_is_dickey' returns False if 'key' is in 'dic'"""
        dic = {"key": "value"}
        self.assertFalse(self.client._is_dickey(dic, "foo"))

    def test_merge_dict(self):
        d1 = {"foo": "bar", "oof": "rab", "fee": "fie"}
        d2 = {"foo": "bar", "oof": "bar"}
        d = {"foo": "bar", "oof": "bar", "fee": "fie"}
        self.assertEqual(self.client._merge_dict(d1, d2), d)

    def test_merge_dict_invalid_dict(self):
        d1 = None
        d2 = None
        with self.assertRaises(TypeError):
            self.client._merge_dict(d1, d2)

    def test_merge_dict_with_one_invalid_dict(self):
        d1 = {"foo": "bar"}
        d2 = None
        self.assertEqual(self.client._merge_dict(d1, d2), d1)
        self.assertEqual(self.client._merge_dict(d2, d1), d1)

    def test_merge_dict_empty_dict(self):
        d1 = {"foo": "bar"}
        d2 = {}
        self.assertEqual(self.client._merge_dict(d1, d2), d1)

    def test_merge_dict_kwargs(self):
        d1 = {"foo": "bar"}

        def foobarteam(dic, **kwargs):
            self.assertEqual(self.client._merge_dict(d1, kwargs), dic)

        foobarteam(d1)
        foobarteam({**d1, **{"op": "login"}}, op="login")
