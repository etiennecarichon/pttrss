from unittest import TestCase
from unittest.mock import Mock, patch
from urllib.parse import urlparse

from pttrss.client import TTRSSSessionHandler, API_PATH
from pttrss.exceptions import TTRSSLoginError, TTRSSConnectionError

URL = "http://rss.unittest.info"
TTRSS_API = URL + API_PATH
FAKE_SID = "xxxxxxxxxxxxxxxxxxxxxxxxxx"


class TestTTRSSSessionURLFormater(TestCase):
    def setUp(self):
        self.tt = TTRSSSessionHandler(URL, "login", "password")
        self.valids_urls = (
            "http://rss.unittest",
            "http://www.rss.unittest",
            "http://rss.unittest/pathtottrss",
            "http://www.rss.unittest/pathtottrss",
            "https://rss.unittest",
            "https://www.rss.unittest",
            "https://rss.unittest/pathtottrss",
            "https://www.rss.unittest/pathtottrss",
        )
        self.miss_scheme = [u.split("://")[1] for u in self.valids_urls]
        self.miss_host = [u.split("/")[2] for u in self.valids_urls]

    def test_url_formater_arguments(self):
        """_url_formater accept url as a urllib.parse.urlparse object."""
        with self.assertRaises(TypeError):
            self.tt._url_formater(URL)

    def test_url_formater_with_missing_scheme(self):
        """_url_formater shall return KeyError if missing scheme like http/s"""
        with self.assertRaises(KeyError):
            for ums, umh in zip(self.miss_scheme, self.miss_host):
                self.tt._url_formater(urlparse(ums))
                self.tt._url_formater(urlparse(umh))

    def test_url_formater_with_valids_urls(self):
        """_url_formater returns an url pointing to tt-rss API"""
        for url in self.valids_urls:
            expect_url = url + API_PATH
            self.assertEqual(self.tt._url_formater(urlparse(url)), expect_url)


class TestTTRSSGetSessionID(TestCase):
    def setUp(self):
        self.tt = TTRSSSessionHandler(URL, "login", "password")
        self.login_error = {
            'seq': 0,
            'status': 1,
            'content': {
                'error': 'LOGIN_ERROR'
            }
        }
        self.valid_resp_sid = {"content": {"session_id": FAKE_SID}}

    @patch("pttrss.client.TTRSSClient._data_parser")
    def test_with_valid_session_id(self, mock_data):
        """get_session_id must return a string from a dictionary containing the
        session ID. This test the case where a valid sid is returned"""
        mock_data.return_value = self.valid_resp_sid
        sid = self.tt.get_session_id(TTRSS_API)
        self.assertEqual(sid, FAKE_SID)

    @patch("pttrss.client.TTRSSClient._data_parser")
    def test_with_invalids_creds(self, mock_data):
        """When passing invalids credentials, TTRSSLoginError is raised"""
        mock_data.return_value = self.login_error
        with self.assertRaises(TTRSSLoginError):
            self.tt.get_session_id(TTRSS_API)


class TestTTRSSIsHostReacheable(TestCase):
    def setUp(self):
        self.tt = TTRSSSessionHandler(URL, "login", "password")

    @patch("http.client.HTTPConnection")
    def test_host_is_reachable(self, mock_http):
        """When host is reachable '_is_host_reachable' returns True"""
        mock_http.request.return_value = None
        self.assertTrue(self.tt._is_host_reachable())

    @patch("http.client.HTTPConnection")
    def test_host_is_not_reachable(self, mock_http):
        """'When host is not reachable, '_is_host_reachable' shall return 
        gaierror
        """
        from socket import gaierror
        from urllib.parse import ParseResult

        url = Mock(spec=ParseResult)
        url.netloc = "\\]{^@].fr"
        mock_http.return_value = gaierror

        with self.assertRaises((gaierror, TTRSSConnectionError)):
            self.tt._is_host_reachable(url)
