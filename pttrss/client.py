#!/usr/bin/env python3

import json

from http.client import HTTPConnection
from socket import gaierror
from urllib import error, request
from urllib.parse import ParseResult, urlparse

from pttrss.exceptions import TTRSSConnectionError, TTRSSLoginError, TTRSSMissingAuthentification, TTRSSMissingOP

CONTENT = ("Content-Type", "application/json")
USERAGENT = ("User-Agent", "ttrss_subscribe.py/1.0")
ENCODING = "utf-8"

API_CONTENT = "content"
API_STATUS = "status"
API_PATH = "/api/"


class TTRSSSessionHandler:
    """Connect to a tt-rss instance, parse methods to it and handle responses.
    You need to allow access to API from your tt-rss account settings.

    API is statefull, you need to login and maintain a session ID (sid) to 
    perform further operations.
    """
    def __init__(self, url, user, password):
        """Class attributes and methods.
        :url: URL to tt-rss instances
        :user: tt-rss login
        :password: tt-rss password
        """
        self.url, self.user, self.password = urlparse(url), user, password
        self.ttrss_api = self._url_formater(self.url)

    def _is_host_reachable(self, url=None, timeout=5):
        """Returns True if the distant server is online.
        :url: urllib.parse.urlparse object pointing to tt-rss instance
        :timeout: request timeout in second
        """
        if not url:
            url = self.url

        if not isinstance(url, ParseResult):
            raise TypeError

        host = url.netloc
        if host == "":
            raise KeyError("URL has no valid host")

        for port in (80, 443):
            connection = HTTPConnection(host=host, port=port, timeout=timeout)
            try:
                connection.request("HEAD", "/")
                return True
            except gaierror:
                raise TTRSSConnectionError("Failed to connect to tt-rss")
            finally:
                connection.close()
        return False

    def _url_formater(self, url=None):
        """Returns a formatted URL to point to tt-rss API.
        :url: urllib.parse.urlparse object pointing to tt-rss instance
        """
        if not url:
            url = self.url

        if not isinstance(url, ParseResult):
            raise TypeError
        if url.scheme == "":
            raise KeyError("Missing scheme in parsed URL (http or https ?)")
        if url.netloc == "":
            raise KeyError("Missing host in parsed URL")
        return url.scheme + "://" + url.netloc + url.path + API_PATH

    def get_session_id(self, ttrss_api=None, login=None, password=None):
        """Login and returns a Session ID needed to perform further operations
        ttrss_api
        :ttrss_api: url pointing to tt-rss API
        :login: tt-rss login
        :password: tt-rss password
        """
        if not ttrss_api:
            ttrss_api = self.ttrss_api
        if not login:
            login = self.user
        if not password:
            password = self.password

        resp = TTRSSClient._data_parser(ttrss_api,
                                        op="login",
                                        user=login,
                                        password=password)

        try:
            sid = resp["content"]["session_id"]
            self.sid = sid
            return sid

        except KeyError:
            raise TTRSSLoginError

    def __enter__(self):
        if self._is_host_reachable():
            self.get_session_id()
            return self
        raise TTRSSConnectionError("Failed to connect to tt-rss")

    def __exit__(self, exc_type, exc_value, traceback):
        return TTRSSClient._data_parser(self.ttrss_api,
                                        sid=self.sid,
                                        op="logout")


class TTRSSClient:
    """Parse methods to tt-rss instance.
    Numeric values must be strings.
    Empty string, numeric zero, unquoted false literal (?), string literal "f"
    or "false" are considered as boolean False. Everything else is True.

    In April 2022, valid methods are : "getApiLevel", "getVersion", "login", 
    "logout", "isLoggedIn", "getUnread", "getCounters", "getFeeds", 
    "getCategories", "getHeadlines", "updateArticle", "getArticle", 
    "getConfig", "updateFeed", "getPref", "catchupFeed", "getCounters", 
    "getLabels", "setArticleLabel", "shareToPublished", "subscribeToFeed", 
    "unsubscribeFeed", "getFeedTree".

    Full API Reference with methods and parameters is available at 
    https://tt-rss.org/wiki/ApiReference.
    """
    def __init__(self, ttrss_session, **kwargs):
        self.ttrss_session = ttrss_session

    def _is_dickey(cls, dic, key):
        """Returns True if 'key' is a dictionary 'dic' key"""
        try:
            dic[key]
        except KeyError:
            return False
        return True

    def _handle_status(self, data):
        """Read tt-rss API response status and returns True or False if it
        succed or not"""
        if type(data) is not dict:
            raise TypeError

        try:
            status_code = data[API_STATUS]
        except KeyError:
            return False
        else:
            if status_code == 0:
                return True
        return False

    def _handle_content(self, data):
        try:
            data[API_CONTENT]
        except KeyError:
            pass
        else:
            return data[API_CONTENT]

    def _test_requirements(self, data, sid=None):
        """Methods and parameters for the HTTP request must fullfill some 
        requierements :
        - Having user and password or a SID
        - Having a OP code
        '_test_requirements' returns True if 'data' meets all the requirement
        """
        b_data = {"op": False, "sid": False, "user": False, "password": False}
        for i in data.keys():
            b_data[i] = data.get(i)
        if sid:
            b_data["sid"] = sid

        if not b_data["op"]:
            raise TTRSSMissingOP
        if not b_data["sid"] and not (b_data["user"] and b_data["password"]):
            raise TTRSSMissingAuthentification
        return True

    def _merge_dict(self, dic1, dic2):
        """_merge_dict  merge dictionnaries with priority : dic2 > dic1"""
        if not isinstance(dic1, dict) and not isinstance(dic2, dict):
            raise TypeError
        elif isinstance(dic1, dict) and not isinstance(dic2, dict):
            return dic1
        elif isinstance(dic2, dict) and not isinstance(dic1, dict):
            return dic2
        else:
            return {**dic1, **dic2}

    @classmethod
    def _data_parser(cls, url, sid=None, data=None, **kwargs):
        """Send methods to tt-rss API and returns response as JSON data.
        :url: URL pointing to tt-rss api.
        :sid: a string containing session ID. Session ID is optionnal for a few
        methods like login, logout, isLoggedIn.
        :data: dictionnary of methods and parameters to send.
        Informations can also be set as function arguments
        dictionnary ('data' argument).
        """
        data = cls._merge_dict(cls, data, dict(kwargs))
        cls._test_requirements(cls, data, sid)
        if sid:
            data["sid"] = sid

        data_post = json.dumps(data).encode(ENCODING)
        opener = request.build_opener()

        opener.addheaders = [CONTENT, USERAGENT]

        try:
            with opener.open(url, data_post) as response:
                return json.loads(response.readlines()[0].decode(ENCODING))
        except error.HTTPError:
            return False

    def request(self, data=None, **kwargs):
        ttrss = self.ttrss_session

        data = self._merge_dict(data, dict(kwargs))
        self._resp = self._data_parser(ttrss.ttrss_api, ttrss.sid, data)
        self.status = self._handle_status(self._resp)
        self.content = self._handle_content(self._resp)

        return self.status, self.content
