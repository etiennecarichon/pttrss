class TTRSSConnectionError(Exception):
    pass

class TTRSSLoginError(Exception):
    pass

class TTRSSMissingAuthentification(Exception):
    pass

class TTRSSMissingOP(Exception):
    pass
