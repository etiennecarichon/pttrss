# `pttrss` - A small Tiny Tiny RSS Python interface

`pttrss` is a simple Python library aiming at easily connect to a tt-rss instances.

Please before any use be sure that external API access is allowed from your instance parameters dashboard.

API References can be read at [tt-rss Wiki](https://tt-rss.org/wiki/ApiReference).

## Examples

    from pttrss import TTRSSSessionHandler, TTRSSClient


    tt = TTRSSSessionHandler("url", "user", "password")
    tt.get_session_id()
    req = TTRSSClient(tt).request(op="subscribeToFeed", feed_url="myrss.xml")
    req.request(op="logout")


`pttrss` can also be use within a context manager :

    from pttrss import TTRSSSessionHandler, TTRSSClient


    with TTRSSSessionHandler("https://ttrss.org", "user", "password") as tt:
        client = TTRSSClient(tt)
        client.request(op="getFeedTree")


# Notes

[ttrss-python](https://github.com/Vassius/ttrss-python) is an other great tool to connect to a tt-rss instance.
